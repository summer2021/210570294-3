`content_md5` carries valid `Content MD5` for the operation.

## Supported Operations

- [Write](go-storage/operations/storager/write.md)
