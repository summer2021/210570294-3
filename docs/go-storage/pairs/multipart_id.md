`multipart_id` is used to specific multipart id of an object.

## Supported Operations

- [Create](go-storage/operations/storager/create.md)
- [Stat](go-storage/operations/storager/stat.md)
- [Delete](go-storage/operations/storager/delete.md)