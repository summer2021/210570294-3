---
slug: /
title: Introduction
---

`Beyond Storage` is a storage abstraction beyond the existing storage services. 

We currently maintain the following projects:

- Library
    - [go-storage](https://github.com/beyondstorage/go-storage): The go version of Beyond Storage.
    - `go-services-*`: The services implements of go-storage.
    - [rs-storage](https://github.com/beyondstorage/rs-storage): The rust version of Beyond Storage.
- Application
    - [dm](https://github.com/beyondstorage/dm/): The data transfer services based on Beyond Storage.