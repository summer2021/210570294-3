---
slug: /
title: 介绍
---

Here are community-related pages for `Beyond Storage`.

## Tips

- Be **open**. Please don't try to connect or ask questions privately.
- Be **nice**. Please try to understand others from the perspective of kindness.
- Be **async**. Please respect each other's time and try to complete the communication asynchronously.

## Discussions

### Async Discussion

We host our async discussion on forums: <https://forum.beyondstorage.io/>.

Our formal announcement will be on our forums.

### Realtime Discussion

#### Matrix

This is the main method to communicate with Beyond Storage developers.

We host our matrix server on `https://chat.aos.dev`.

- For english speaking: join in [#campfire:aos.dev](https://matrix.to/#/#campfire:aos.dev)  <!-- Need Update. -->
- For chinese speaking: join in [#campfire-zh:aos.dev](https://matrix.to/#/#campfire-zh:aos.dev) <!-- Need Update. -->

#### Slack

We have linked our matrix room to slack. Please join our slack via <https://join.slack.com/t/aos-wg/shared_invite/zt-pf4yk96u-7znnJVYpQvY57C3rRrbDPg> <!-- Need Update. -->

#### Discord

We have linked our matrix room to discord.

- For english speaking: join in [#campfire](https://discord.gg/zFSRRDDhAD)
- For chinese speaking: join in [#campfire-zh](https://discord.gg/pE26rCUNkb)

## 活动

Please visit [events](events/index.md) to got more info.

