---
title: 介绍
sidebar_position: 1
---

## Create

[Create](go-storage/operations/servicer/create.md) will create a new `Storage` under service.

## Delete

[Delete](go-storage/operations/servicer/delete.md) will delete a `Storage` under service.

## Get

[Get](go-storage/operations/servicer/get.md) will get an already exist `Storage` from service.

## List

[List](go-storage/operations/servicer/list.md) will list all `Storage` under service.
