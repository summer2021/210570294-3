`create` is used to create an Object without any API call.

## Behavior

- `create` SHOULD NOT do any API call.