`size` is used to limit the read size.

## Supported Operations

- [Read](go-storage/operations/storager/read.md)
