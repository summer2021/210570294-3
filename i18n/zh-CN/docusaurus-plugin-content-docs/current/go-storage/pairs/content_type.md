`content_type` carries valid `Content Type` for the operation.

## Supported Operations

- [Write](go-storage/operations/storager/write.md)
