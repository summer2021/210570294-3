---
slug: /
title: 介绍
---

`Beyond Storage` is a storage abstraction beyond the existing storage services.

我们目前正在维护以下项目：

- 库
    - [go-storage](https://github.com/beyondstorage/go-storage): The go version of Beyond Storage.
    - `go-services-*`: go-storage 不同服务的实现。
    - [rs-storage](https://github.com/beyondstorage/rs-storage): The rust version of Beyond Storage.
- 应用
    - [dm](https://github.com/beyondstorage/dm/): The data transfer services based on Beyond Storage.